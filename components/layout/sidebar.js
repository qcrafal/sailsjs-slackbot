import React from 'react';
import UserInfo from './UserInfo';
import Logout from '../Logout'

export default class Sidebar extends React.Component {
  render() {
    return (
      <div className="sidebar">
        <UserInfo />
        <Logout />
      </div>
    );
  }
}
