import React from 'react';
import Sidebar from './sidebar';
import Content from './content';

export default class Layout extends React.Component {
  render() {
    return (
      <div className="layout">
        <Sidebar />
        <Content />
      </div>
    );
  }
}
