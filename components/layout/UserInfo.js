import React from 'react';
import User from '../User'

export default class UserInfo extends React.Component {
  render() {
    return (
      <div className="user-info">
        <User source="/user/me"/>
      </div>
    );
  }
}
