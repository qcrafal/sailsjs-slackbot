import React from 'react';

import Dashboard from './dashboard';

export default class Content extends React.Component {
  render() {
    return (
      <div className="content">
        <Dashboard />
      </div>
    );
  }
}
