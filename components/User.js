import React from "react";
import UserForm from "./forms/UserForm";
import Joke from './Joke';

import _ from 'lodash';

export default class User extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formShown: false,
      user: {}
    };
  }

  showForm() {
    this.setState({
      formShown: true,
    });
  }

  hideForm(user) {
    this.setState({
      formShown: false,
      user
    });
  }

  componentDidMount() {
    fetch(this.props.source, {
      credentials: 'same-origin',
    })
      .then(response => response.json())
      .then(user => {
        this.setState({user});
      });
  }

  render() {
    const component = <div
      className="user">
      <div className="greeting">
        Hello { this.state.user.username }
        ({this.state.user.firstName} {this.state.user.lastName})

        <Joke user={this.state.user} />

        <p>Actions</p>
        <ul className="actions">
          <li>
            {(editFormShown => {
              if (!editFormShown) {
                return (<button onClick={this.showForm.bind(this)}>Edit</button>);
              }
            })(this.state.formShown)}
          </li>
        </ul>

        {(editFormShown => {
          if (editFormShown) {
            return (<UserForm user={this.state.user} onUserEdited={this.hideForm.bind(this)}/>)
          }
        })(this.state.formShown)}
      </div>
      <Logout />
    </div>;

    const placeholder = <div>...</div>;

    return _.get(this.state, 'user.username') ? component : placeholder;
  }
}
