import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './layout/main';

ReactDOM.render(<Layout />, document.getElementById('container'));
