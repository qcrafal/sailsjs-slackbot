import React from 'react';

export default class Joke extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      joke: ''
    };
  }

  componentDidMount() {
    let url = 'https://api.icndb.com/jokes/random';

    const user = this.props.user;

    if(user.firstName && user.lastName) {
      url += '?firstName='+ user.firstName + '&lastName=' + user.lastName;
    }

    fetch(url)
      .then(response => response.json())
      .then(data => {
        this.setState({
          joke: data.value.joke
        });
      });
  }

  render() {
    return <div className="joke">{ this.state.joke }</div>;
  }

}
