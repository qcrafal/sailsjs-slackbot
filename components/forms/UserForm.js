import React from "react";

const {
  alert,
  Request,
} = window;

export default class UserForm extends React.Component {
  constructor(...props) {
    super(...props);

    this.state = {
      user: Object.assign({}, this.props.user),
    };
  }

  handleSubmit(event) {
    event.preventDefault();

    const request = new Request(`/user/${ this.state.user.id }`, {
      credentials: 'same-origin',
      method: 'PUT',
      body: JSON.stringify(this.state.user),
    });

    fetch(request)
      .then(response => response.json())
      .then(data => {
        if (typeof this.props.onUserEdited === 'function') {
          this.props.onUserEdited(data);
        }
      });
  }

  handleFiledChange(event) {
    this.setState({
      user: Object.assign({}, this.state.user, {
        [event.target.name]: event.target.value
      }),
    });
  }

  render() {
    return <form
      name="userForm"
      onSubmit={event => this.handleSubmit(event)}>
      <input type="text"
        name="firstName"
        placeholder="First Name"
        defaultValue={this.props.user.firstName}
        onChange={this.handleFiledChange.bind(this)}/>
      <input type="text"
        name="lastName"
        placeholder="Last Name"
        defaultValue={this.props.user.lastName}
        onChange={this.handleFiledChange.bind(this)}/>
      <input type="password"
        name="password"
        placeholder="(change) Password"
        defaultValue={this.props.user.password}
        onChange={this.handleFiledChange.bind(this)}/>
      <input type="text"
        name="freckleId"
        placeholder="Freckle ID"
        defaultValue={this.props.user.freckleId}
        onChange={this.handleFiledChange.bind(this)}/>
      <input type="text"
        name="slackId"
        placeholder="Slack ID"
        defaultValue={this.props.user.slackId}
        onChange={this.handleFiledChange.bind(this)}/>
      <input type="email"
        name="email"
        placeholder="Email"
        defaultValue={this.props.user.email}
        onChange={this.handleFiledChange.bind(this)}/>
      <input type="submit" value="Submit" />
    </form>
  }
}
