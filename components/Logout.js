import React from 'react';

export default class Logout extends React.Component {
  render(){
    return <a href="/logout">Logout</a>;
  }
}
