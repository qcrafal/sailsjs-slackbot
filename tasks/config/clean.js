/**
 * `clean`
 *
 * ---------------------------------------------------------------
 *
 * Remove the files and folders in your Sails app's web root
 * (conventionally a hidden directory called `.tmp/public`).
 *
 * For usage docs see:
 *   https://github.com/gruntjs/grunt-contrib-clean
 *
 */
module.exports = function(grunt) {
  var pipeline = require('../pipeline');

  grunt.config.set('clean', {
    dev: ['.tmp/public/**'],
    es6Build: [pipeline.es6To5BuildPath+'/**/*.js', pipeline.typescriptDir + 'dist/**/*.ts'],
    build: ['www']
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
};
