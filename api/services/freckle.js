"use strict";

var https = require( "https" );

const freckle = function() {
  function self( token ) {
    if (!token) {
      throw new Error("Freckle token not passed. Was: '" + token + "'.");
    }
    self.settings.token = token;

    return self;
  }

  self.date = function( obj ) {
    var year = obj.getFullYear()
      , month = obj.getMonth() + 1
      , day = obj.getDate()

    month = month < 10 ? '0' + month : month.toString()
    day = day < 10 ? '0' + day : day.toString()

    return [ year, month, day ].join( '-' );
  };

  self.settings = {
    host: "letsfreckle.com"
    , path: "/v2/"
    , subdomain: "api"
    , port: 443
    , token: "lx3gi6pxdjtjn57afp8c2bv1me7g89j"
    , authHeader: "X-FreckleToken"
  };

  const queryFilters = {
    updated_from: (value) => {
      // freckle requires 'YYYY-MM-DDTHH:MM:SSZ' format
      return new Date(value).toISOString().replace(/\.[0-9]+Z/, 'Z');
    }
  };

  var paramExp = /(:\b\w*\b)/gi;

  function api() {
    var args = Array.prototype.slice.call( arguments )
      , conf = args.shift()
      , path = conf.path
      , method = conf.method || "GET"
      , data = {}
      , requestQuery = '';

    function action() {
      var args = Array.prototype.slice.call( arguments );
      var options = args[0] || {};
      var query = options.query;
      requestQuery = '';
      if (query) {
        if (Object.keys(query).length) {
          requestQuery = "?";
        }
        for (var key in query) {
          let queryValue;
          if (query[key]) {
            if (typeof queryFilters[key] === 'function') {
              queryValue = queryFilters[key](query[key]);
            } else {
              queryValue = query[key];
            }
            requestQuery += key + "=" + queryValue + "&";
          }
        }
      }

      if( !paramExp.test(path) ) {
        if( typeof options.data == "object" ) {
          data = options.data;
        }
        return action.request.call( self, args[1] );
      }

      path.match( paramExp ).forEach(function( match ) {
        if( typeof match == "function" ) {
          throw new Error( "Incorrect argument type" );
        }

        if( typeof match == "object" ) {
          data = match;
          return;
        }

        path = path.replace( match, args.shift() );
      });
      return action.request.apply( self, args[args.length - 1] );
    }

    action.request = function( cb ) {
      if( !cb ) { return; }

      var args = Array.prototype.slice.call( arguments );

      var options = {
        host: this.settings.subdomain + '.' + this.settings.host
        , port: this.settings.port
        , path: this.settings.path + path + requestQuery
        , method: method
        , headers: {}
      }
        , postdata = '';

      options.headers[ this.settings.authHeader ] = this.settings.token;
      options.headers['User-Agent'] = 'MyFreckleBot/1.0';

      if( data.auth ) {
        options.headers[ "Authorization" ] = "Basic " + new Buffer(
            data.auth[0] + ':' + data.auth[1] ).toString( "base64" );

        delete data.auth;
      }

      if( Object.keys(data).length ) {
        postdata = JSON.stringify( data ) + '\r\n';
        options.headers[ "Content-Type" ] = "application/json";
        options.headers[ "Content-Length" ] = postdata.length;
      }
      var req = https.request(options, function(res) {
        var ret = '';

        res.on( "data", function( chunk ) { ret += chunk; });

        res.on( "end", function() {
          if( res.statusCode === 401 || res.statusCode === 422 ) {
            cb( res );
          }
          cb( 0, ret.length > 1 ? JSON.parse(ret) : '' );
        });

      }).on('error', function(e) {
        cb( e.message );
      });

      postdata && req.write( postdata );

      req.end();
    };

    return action;
  }

  self.entries = {
    list: api({ path: "entries" })
    , add: api({ path: "entries.json", method: "POST" })
    , import: api({ path: "entries/import.json" })
  };

  self.projects = {
    list: api({ path: "projects.json" })
    , add: api({ path: "projects.json", method: "POST" })
  };

  self.tags = {
    list: api({ path: "tags.json" })
  };

  self.users = {
    list: api({ path: "users" })
    , show: api({ path: "users/:id" })
    , token: api({ path: "user/api_auth_token.json" })
    , add: api({ path: "users", method: "POST" })
    , update: api({ path: "users/:id.json", method: "PUT" })
    , remove: api({ path: "users/:id.json", method: "DELETE" })
  };

  return self;
}();

module.exports = freckle(sails.config.freckle.api_key);
