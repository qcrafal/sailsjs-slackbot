const Botkit = require('botkit');

const controller = Botkit
  .slackbot();

controller
  .spawn({
    token: sails.config.slack.api_key
  })
  .startRTM(err => {
    if (err) {
      sails.log.error(err);
    }
  });

controller.hears(
  'bry',
  ['direct_message', 'direct_mention', 'mention'],
  (bot, message) => {
    bot.reply(message, 'Hello yourself.');
  }
);

module.exports = controller;
