module.exports = (req, res, next) => {

  const is_auth = req.isAuthenticated();

  if (is_auth) {
    // User is allowed, proceed to controller
    return next();
  }
  else {
    // User is not allowed
    return res.redirect("/login");
  }

};
