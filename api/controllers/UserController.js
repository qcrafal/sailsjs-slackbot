/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index(req, res) {
    User
      .find(req.allParams())
      .exec((err, collection) => {
        if (err) {
          res.send(500, 'error fetching user list');
        }
        res.send(collection);
      })
  },

  me(req, res) {
    res.send(req.user);
  },
};

