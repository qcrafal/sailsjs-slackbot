/**
 * TestController
 *
 * @description :: Server-side logic for managing Tests
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * GET /freckle/users
   *
   * @param req
   * @param res
   */
  users(req, res){

    freckle.users.list(null, function (err, users) {
      if(err) {
        res.send(err);
      }
      res.send(users);
    });

  },

   /**
   * GET /freckle/entries
   *
   * @param req
   * @param res
   */
  entriesWithParams(req, res) {
    var query = {};
    var userId = req.params.user_id;
    if (userId) {
      query.user_ids = userId;
    }
    var projectId = req.params.project_id;
    if (projectId) {
      query.project_ids = projectId;
    }
    var queryFrom = req.query.from;
    if (queryFrom) {
      query.updated_from = queryFrom;
    }

    freckle.entries.list({
      query: query
    }, function (err, entries) {
      if(err) {
        res.send(err);
        return;
      }
      res.send(entries);
    });
  }


};

