/**
 * AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const passport = require('passport');

module.exports = {

  /**
   * GET /login
   * GET /auth/login
   *
   * @param req
   * @param res
   */
  login(req, res) {
    res.view();
  },

  /**
   * POST /login
   * POST /auth/login
   *
   * @param req
   * @param res
   */
  process(req, res){
    passport.authenticate('local', (err, user, info) => {

      if ((err) || (!user)) {
        return res.send({
          message: 'login failed'
        });
      }

      req.logIn(user, (err) => {
        if (err) res.send(err);
        return res.redirect('/dashboard');
      });
    })(req, res);
  },

  /**
   * GET /logout
   * GET /auth/logout
   *
   * @param req
   * @param res
   */
  logout(req, res){
    req.logout();
    res.send('logout successful');
  }
};
