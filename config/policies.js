const _ = require('lodash');
/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */

const authenticatedPolicy = 'authenticated';

const policyGroups = {
  common: [
    authenticatedPolicy
  ],
  unprotected: _.without(this.common, authenticatedPolicy)
};

module.exports.policies = {
 '*': policyGroups.common,

  AuthController: {
    // Allow logging in without being logged in. For now the only unsecured
    // action.
    'login': policyGroups.unprotected,
    'process': policyGroups.unprotected,
  },
};
